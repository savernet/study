package com.first.study.A.Controller;

import com.first.study.Conf.ResUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/A")
public class AController {
    @GetMapping("/")
    public ResponseEntity test(){
        log.info("info");
        log.debug("debug");
        log.error("error");
        return ResUtil.success("ttt");
    }
}
