package com.first.study.Conf;

import com.first.study.Com.vo.ResBody;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class ResUtil {
    public static <T> ResponseEntity<ResBody> success(T t) {
        Date now = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ResBody res = new ResBody(sdf.format(now), HttpStatus.OK.value(), t, "", "");
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    public static ResponseEntity<ResBody> error(HttpStatus status, Object msg, String errorCode, String error) {
        Date now = new Date(System.currentTimeMillis());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ResBody res = new ResBody(sdf.format(now), status.value(), msg, error, errorCode);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }
}