package com.first.study.Com.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResBody {
    String time;
    int code;
    Object msg;
    String error;
    String errCode;
}
